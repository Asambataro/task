import pandas as pd
import time
import os
import datetime

#Check Validità dati inseriti

def validate(date_text):
	try:
		datetime.datetime.strptime(date_text, '%d/%m/%Y')
		return True
	except ValueError:
		print ("Incorrect data format, should be DD-MM-YYYY '%s'" %date_text)
		return False
		

#Hello 

print('Hello Mate, welcome to the first Samba task on python\n \r')
print('Adesso sto generando un file CSV, a breve lo vedrai sulla shell\n \r')

# Tempo random

time.sleep(1.5)
print('1.5 seconds have passed\n \r')

# Generate CSV File

names = pd.DataFrame([['Antonio', 'Sambataro'], ['Davide', 'Zagami']], columns=['Nome', 'Cognome'])
names.to_csv('names.csv', index=False)


#Display the generated CSV

data = pd.read_csv('names.csv')
print(data)

#Enter data from keyboard

print('\n \r')
print('Data Input Zone')
print('\n \r')
input_text1 = input ("Inserisci nuova colonna 'Es. Data Nascita': ")
input_text2 = input ("Inserisci Valore 'Prima Riga'%s: " %input_text1)
input_text3 = input ("Inserisci Valore 'Seconda Riga'%s: "%input_text1)

#check errore data
checkval = validate (input_text2)
checkval1 = validate (input_text3)

if checkval == True :
	# Tempo random
	print('Stiamo generando nuovo csv\n \r')
	time.sleep(1.5)
	print('1.5 seconds have passed\n \r')

	#New CSV 
	newcsv = pd.DataFrame([['Antonio', 'Sambataro', '%s'%input_text2], ['Davide', 'Zagami', '%s'%input_text3]], columns=['Nome', 'Cognome', '%s'%input_text1])
	newcsv.to_csv('newcsv.csv', index=False)
	# Print new CSV
	datanew = pd.read_csv('newcsv.csv')
	print(datanew)

else:
	#New CSV 
	newcsv = pd.DataFrame([['Antonio', 'Sambataro','ERROR'], ['Davide', 'Zagami', '%s'%input_text3]], columns=['Nome', 'Cognome', '%s'%input_text1])
	newcsv.to_csv('newcsv.csv', index=False)
	# Print new CSV
	datanew = pd.read_csv('newcsv.csv')
	print(datanew)

if checkval1 == True :
	# Tempo random
	print('Stiamo generando nuovo csv\n \r')
	time.sleep(1.5)
	print('1.5 seconds have passed\n \r')

	#New CSV 
	newcsv = pd.DataFrame([['Antonio', 'Sambataro', '%s'%input_text2], ['Davide', 'Zagami', '%s'%input_text3]], columns=['Nome', 'Cognome', '%s'%input_text1])
	newcsv.to_csv('newcsv.csv', index=False)
	# Print new CSV
	datanew = pd.read_csv('newcsv.csv')
	print(datanew)

else:
	#New CSV 
	newcsv = pd.DataFrame([['Antonio', 'Sambataro', '%s'%input_text2], ['Davide', 'Zagami', 'ERROR']], columns=['Nome', 'Cognome', '%s'%input_text1])
	newcsv.to_csv('newcsv.csv', index=False)
	# Print new CSV
	datanew = pd.read_csv('newcsv.csv')
	print(datanew)

#Delete OLD CSV
os.remove("names.csv")





